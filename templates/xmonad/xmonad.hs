
import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO

myTerminal          = "terminator"
rootTerminal        = "'terminator -p root'"
myWorkspaceBar      = "xmobar /home/ra2er/.xmobarrc" 
myWorkspaces        = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
myFocusFollowsMouse = True 

myBorderWidth        = 1
myNormalBorderColor  = "#000000"
myFocusedBorderColor = "green"

myManageHook = composeAll
    [ className =? "MPlayer"      --> doFloat
    , className =? "Gimp"         --> doFloat
    , className =? "Gajim"        --> doFloat]
    
myLayout = avoidStruts $ layoutHook defaultConfig 

main = do 
    xmproc <- spawnPipe myWorkspaceBar
    xmonad $ defaultConfig
        { terminal              = myTerminal
        , modMask               = mod4Mask
        , workspaces            = myWorkspaces
        , normalBorderColor     = myNormalBorderColor
        , focusedBorderColor    = myFocusedBorderColor
        , focusFollowsMouse     = True 
        , manageHook            = myManageHook 
        , logHook               = dynamicLogWithPP $ xmobarPP 
            { ppOutput  = hPutStrLn xmproc
            , ppTitle   = xmobarColor "green" "" . shorten 100}
        , layoutHook            = myLayout}
